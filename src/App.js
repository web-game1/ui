import React, {useReducer} from "react";
import {Login} from "./components/Login";
import {Chat} from "./components/Chat";
import reducer from "./reducer";


function App() {
    const [state , dispatch] = useReducer(reducer , {
        isAuth : false
    })
    const onLogin = () => {
        dispatch({
            type : "IS_AUTH",
            payload : true
        })
    }
    return (
        <>
            {state.isAuth ?
                <Chat/> :
                <Login onLogin={onLogin}/>
            }
        </>
    );
}

export default App;