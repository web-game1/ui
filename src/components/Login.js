import React, {useState} from "react";
import axios from "axios";

export const Login = ({onLogin}) => {
    const [roomId, setRoomId] = useState('')
    const [userName, setUserName] = useState('')
    const [isLoading, setLoading] = useState(false)

    const onEnter = async  () => {

        console.log(userName , roomId)
        if (!roomId || !userName) {
            return alert(!roomId ? 'enter room id' : 'enter user name')
        }
        setLoading(true)
        await axios.post('/rooms' , {
            user : userName,
            room : roomId
        }).then(onLogin)

    }

    return (
        <div>
            <input type="text"
                   placeholder='ID room'
                   value={roomId}
                   onChange={({target})=>setRoomId(target.value)}/>
            <input type="text"
                   placeholder='Name'
                   value={userName}
                   onChange={({target})=>setUserName(target.value)}/>
            <button disabled={isLoading} onClick={onEnter}>Connect Socket</button>
        </div>
    );
}