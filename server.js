const express = require('express');
const app = express();
const httpServer = require('http').createServer(app);
const {Server} = require("socket.io");
const io = new Server(httpServer);
const port = 9999;
const rooms = new Map();

app.use(express.json())

app.get('/rooms', (req, res) => {
    res.json(rooms)
    console.log(req)
    console.log(res)
});

app.post('/rooms', (req, res) => {
    const {room, user} = req.body

    console.log(room , user)
    if (!rooms.has(room)) {
        rooms.set(
            room,
            new Map([
                ["users", new Map()],
                ["messages", []]
            ]))
    }
    res.send()
})

console.log(io)

io.on('connection', (socket) => {
    console.log('a user connected');
    console.log(socket.id)
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

httpServer.listen(port, () => {
    console.log(`listening on *:${port}`);
});